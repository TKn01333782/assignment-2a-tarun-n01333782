﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="database.aspx.cs" Inherits="Assginment_2a.database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="row">
       <div class="col-md-6">
           <h4>DATABASE (SQL)</h4>
            <p>
               <strong>by Simon Borer </strong>
            </p>
        </div>
        <div class="col-md-6">
           <h4>Module 1</h4>
             <p>This week we're starting! We're going to start by reviewing the course outline. After that, we'll learn: what a database is, and how they're used in web development; what SQL is; how to set up SQL Developer so that we can use the Oracle RDBMS; basic SQL terms definitions; simple single-table queries.
           </p>
       </div>
        <div class="col-md-6">
           <h3>Module 2</h3>
             <p>This week we'll cover retrieving rows from a single table; implementing arithmetic statements; renaming columns in results; retrieving rows using comparison operators; using DISTINCT to eliminate duplicate rows; defining the logical operators AND, OR and NOT; retrieving rows using LIKE, BETWEEN, IN and IS NULL operators; sorting the result set using ORDER BY.
           </p>
        </div>
        <div class="col-md-6">
           <h4>Module 2</h4>
             <p>This week we'll cover retrieving rows from a single table; implementing arithmetic statements; renaming columns in results; retrieving rows using comparison operators; using DISTINCT to eliminate duplicate rows; defining the logical operators AND, OR and NOT; retrieving rows using LIKE, BETWEEN, IN and IS NULL operators; sorting the result set using ORDER BY.
           </p>
        </div>
     </div>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="page1" runat="server">
</asp:Content>




