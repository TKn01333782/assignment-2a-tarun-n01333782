﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="web_programming.aspx.cs" Inherits="Assginment_2a.web_programming" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   <div class="row">
       <div class="col-md-6">
           <h2>WEB PROGRAMMING</h2>
            <p>
              <strong>By Sean Doyle</strong> 
            </p>
      
       </div>
        <div class="col-md-6">
           <h2>Week 1</h2>
            <p>1 – Semantic Coding</p>
             <p>Semantic coding means using the proper element that describes the data it
                contains. A <!--<p><b>--> should not be used for headings; a <!--<table>--> should contain a
                data chart and not be used for layout.
             </p>
            </div>
        <div class="col-md-6">
          <h2>Week 2</h2>
            <p><strong>2 – Variables & Operators.</strong></p>
            <p>Jenn is new to bowling but hopes to join the bowling team. For the tryout she played five
                games with the hopes of making the cutoff average of 185 points to earn a spot on the team.
                She’s fairly certain she didn’t make the team but wants to find out what her average was and
                how far off she was from making the team
            </p>
        </div>
        <div class="col-md-6">
           <h2>week 3</h2>
             <p><strong>3 – Control Structures</strong></p>
                <p>1. Create a popup box that asks for a yes or no answer.</p>
                <p> Output the user’s response to the console.</p>
                <p> Test all possible user options (there are 3) to see what value is returned.</p>
                <p>2. Create a popup that accepts a string of text.</p>
                <p> Test all possible user options to see what value is returned.</p>
                <p> Next, add the following code line and enter 4 in the prompt box.</p>
          </div>
      </div>
    </asp:Content>
  <asp:Content ID="Content2" ContentPlaceHolderID="page3" runat="server">
</asp:Content>
