﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="web_application.aspx.cs" Inherits="Assginment_2a.web_application" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
       <div class="col-md-6">
           <h3>WEB APPLICATION DEVELOPMENT </h3>
            <p>
               <strong>Christine Bittle</strong>
            </p>
        </div>
        <div class="col-md-6">
           <h3>Week 2</h3>
            <ul id="Overview">
                 <li>Introducing Web server controls & their classifications</li>
                 <li>Looking at HTML server controls, such as TextBox, DropDownList, RadioButtonList.</li>
                 <li>Introducing validators and their purpose on a web document.</li>
                 <li>Learning specific validators such as RequiredFieldValidator, RangeValidator, RegularExpressionValidator, ValidationSummary.</li>       
            </ul>
            </div>
        <div class="col-md-6">
          <h3>Week 3</h3>
            <p>This week, we'll be looking at the CodeBehind file. Our web form had the file extension (.aspx), our codebehind will have a different extension (.aspx.cs).
          </p>
        </div>
        <div class="col-md-6">
           <h3>week 4</h3>
             <p>In today's lesson we will be learning what a class is in C#. In addition, we will be looking at our code from assignment 1a and creating instances of our classes called objects. We will then use our objects to store information from our server controls in fields.
         </p>
       </div>
     </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="page2" runat="server">
</asp:Content>
